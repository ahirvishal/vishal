from flask import Flask, render_template
import requests
# import redirect
from bs4 import BeautifulSoup
import pandas as pd
from flask import Flask, render_template, request, redirect, url_for
app = Flask(__name__)

@app.route('/')
def index():

    return render_template('index.html')

@app.route('/process', methods=['POST'])
def process():
    textbox_value = request.form['mytextbox']
    
    print("-----------------------------")
    print(textbox_value)
    
    print("-----------------------------")
    if textbox_value == '':
        return redirect(url_for('process', filename='vishal.css'))
    else:
        # Parse the HTML content of the webpage using BeautifulSoup
        response = requests.get(textbox_value)
        soup = BeautifulSoup(response.content, 'html.parser')

        # Find all the links on the webpage
        links = []
        for link in soup.find_all('a'):
            links.append(link.get('href'))

        # Preprocess the data using Pandas
        df = pd.DataFrame(links, columns=['Link'])
        df.drop_duplicates(inplace=True)
        df.dropna(inplace=True)

        # Render the data in a web app using Flask
        return render_template('index.html', data=df.to_html())
    


    # my_python_function(textbox_value)
    # return 'Value submitted: {}'.format(textbox_value)

    

def my_python_function(text):
    print('The text entered was:', text)

if __name__ == '__main__':
    app.run(debug=True)
